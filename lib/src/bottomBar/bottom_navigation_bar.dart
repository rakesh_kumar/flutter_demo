import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/friendList/friend_list.dart';
import 'package:flutter_challenge/src/login/login.dart';
import 'package:flutter_challenge/src/profile/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomNavigationBarScreen extends StatefulWidget {
  static const tag = 'bottomNavigationBar';
  final String title;
  final bool isBack;
  BottomNavigationBarScreen({Key key, this.title, this.isBack}) : super(key: key);

  @override
  _BottomNavigationBarScreenState createState() => _BottomNavigationBarScreenState();
}

class _BottomNavigationBarScreenState extends State<BottomNavigationBarScreen> {
  bool isSwitched = false;
  var textValue = 'Switch is OFF';
  int _selectedIndex = 0;
  static const TextStyle optionStyle = TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold);
  final List<Widget> _widgetOptions = <Widget>[
    new FriendListScreen(),
    new ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }
  void toggleSwitch(bool value) {
    if(isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    } else {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: Colors.white24,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text('DRAWER'),
              ),
              ListTile(
                title: Column(
                  children: [
                    Row(
                      children: [
                        Text('DARK MODE'),
                      ],
                    ),
                    Switch(
                      onChanged: toggleSwitch,
                      value: isSwitched,
                      activeColor: Colors.blue,
                      activeTrackColor: Colors.yellow,
                      inactiveThumbColor: Colors.redAccent,
                      inactiveTrackColor: Colors.orange,
                    )
                  ],
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: Text('LOGOUT'),
                onTap: () async {
                  SharedPreferences prefer = await SharedPreferences.getInstance();
                  prefer.setString("userName", '');
                  prefer.setString("userImage", '');
                  prefer.setBool("isLoggedIn", false);
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
                          (route) => false);
                },
              ),
            ],
          ),
        ),
      ),
      appBar: AppBar(
        title: _selectedIndex == 0 ? Text("Friend List") : Text("Profile"),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.contacts),
            title: Text('Friend List'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box),
            title: Text('Profile'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: _onItemTapped,
      ),
    );
  }
}
