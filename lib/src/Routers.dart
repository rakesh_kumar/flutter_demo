import 'package:flutter_challenge/src/bottomBar/bottom_navigation_bar.dart';
import 'package:flutter_challenge/src/friendList/friend_list.dart';
import 'package:flutter_challenge/src/login/login.dart';
import 'package:flutter_challenge/src/room/room.dart';
import 'package:flutter_challenge/src/splash/splash.dart';

class Routers {
  static initialRoute() {
    return SplashScreen.tag;
  }

  static allRoutes() {
    return {
      SplashScreen.tag: (context) => SplashScreen(),
      LoginScreen.tag: (context) => LoginScreen(),
      BottomNavigationBarScreen.tag: (context) => BottomNavigationBarScreen(),
      //FriendListScreen.tag: (context) => FriendListScreen(),
      RoomScreen.tag: (context) => RoomScreen(),
    };
  }
}
