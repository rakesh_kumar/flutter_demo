import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/bottomBar/bottom_navigation_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  static const tag = 'login';

  LoginScreen({Key key, this.title, this.isBack}) : super(key: key);
  final String title;
  final bool isBack;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController userIdController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _validateUserId = true;
  bool _validatePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              loginInfo(),
              SizedBox(
                width: 40,
              ),
              buttonHandle()
            ],
          ),
        ),
      ),
    );
  }

  //#Login - Login Info
  Widget loginInfo() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0, right: 12),
            child: TextFormField(
              controller: userIdController,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontFamily: "SFProDisplay-Medium",
                  letterSpacing: .35),
              decoration: new InputDecoration(
                  errorText: _validateUserId ? '' : "Field can't be Empty",
                  errorStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                    fontFamily: "SFProDisplay-Medium",
                    letterSpacing: .35,
                  ),
                  hintText: 'Enter User Id',
                  disabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: "SFProDisplay-Medium",
                  )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0, right: 12),
            child: TextFormField(
              controller: passwordController,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontFamily: "SFProDisplay-Medium",
                  letterSpacing: .35),
              decoration: new InputDecoration(
                  errorText: _validatePassword ? '' : "Field can't be Empty",
                  errorStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                    fontFamily: "SFProDisplay-Medium",
                    letterSpacing: .35,
                  ),
                  hintText: 'Enter Password',
                  disabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  hintStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontFamily: "SFProDisplay-Medium",
                  )),
            ),
          ),
        ],
      ),
    );
  }

  Widget buttonHandle() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 40),
        child: Container(
          height: 45,
          width: 200,
          child: CupertinoButton(
            onPressed: () async {
              bool validUserId =
                  RegExp(r'^[a-zA-Z0-9  ]+$').hasMatch(userIdController.text);
              userIdController.text.trim().isEmpty
                  ? _validateUserId = false
                  : _validateUserId = true;

              passwordController.text.trim().isEmpty ? _validatePassword = false
                  : _validatePassword = true;

              if (validUserId && _validatePassword) {
                SharedPreferences prefer =
                    await SharedPreferences.getInstance();
                prefer.setBool("isLoggedIn", true);
                Navigator.pushNamed(context, BottomNavigationBarScreen.tag);
              }
              else {
                setState(() {});
              }
            },
            padding: EdgeInsets.all(0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "LOGIN",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.deepOrange,
              border: Border.all(width: 1, color: Colors.deepOrange)),
        ),
      ),
    );
  }
}
