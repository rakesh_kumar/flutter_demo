import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with TickerProviderStateMixin, ImagePickerListener {
  TextEditingController nameController = new TextEditingController();
  bool _validName = false;
  File _imageFilePath;
  var image;
  var base64encoded;
  var file;
  Uint8List bytesImage;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  int code;
  var dateFormat = DateFormat("dd MMM yyyy");
  bool editable = false;

  @override
  void initState() {
    super.initState();
    getImagePath().then((value) => {
      _imageFilePath = value,
      userImage(_imageFilePath)
    });
     getSavedName().then((value) => {
       nameController.text = value,
       _validName = true
    });

    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
  }
  Future<String> getSavedName() async {
    SharedPreferences prefer = await SharedPreferences.getInstance();
    return prefer.get("userName");
  }
  Future<File> getImagePath() async {
    SharedPreferences prefer = await SharedPreferences.getInstance();
    String imagePath = prefer.get("userImage");
    File imageFilePath = File(imagePath);
    return imageFilePath;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> savedName() async {
      SharedPreferences prefer =  await SharedPreferences.getInstance() ;
      prefer.setString("userName", nameController.text.trim());
    }

    Future<void> savedImagePath() async {
      SharedPreferences prefer =  await SharedPreferences.getInstance() ;
      prefer.setString("userImage", _imageFilePath.path.toString());
    }

    var initialBlock = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        new Stack(
          children: <Widget>[
            Positioned.fill(
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      imagePicker.showDialog(context);
                    },
                    child: _imageFilePath == null
                        ? CircleAvatar(
                            backgroundImage: bytesImage != null
                                ? MemoryImage(bytesImage)
                                : ExactAssetImage("assets/profile.png"),
                            radius: MediaQuery.of(context).size.height * 0.07,
                          )
                        : CircleAvatar(
                            backgroundImage: new FileImage(_imageFilePath),
                            radius: MediaQuery.of(context).size.height * 0.07,
                          ),
                  ),
                )),
            new Center(
                child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.2)),
                GestureDetector(
                    onTap: () {
                      imagePicker.showDialog(context);
                    },
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.18,
                        ),
                      ],
                    )),
              ],
            )
            ),
          ],
        ),
      ],
    );

    // Save Button widget
    var saveButton = Container(
      height: 44.0,
      width: 120.0,
      child: RaisedButton(
        onPressed: () {
          nameController.text.trim().isEmpty
              ? _validName = false
              : _validName = true;
          setState(() {
          });
          savedImagePath();
          if (_validName) {
            savedName();
            FocusScope.of(context).unfocus();
            Fluttertoast.showToast(
              msg: 'Profile Information Saved successfully',
              textColor: Colors.white,
              fontSize: 16.0,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              backgroundColor: Colors.blue,
            );
          }
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        padding: EdgeInsets.all(0.0),
        child: Container(
          color: Colors.blue,
          alignment: Alignment.center,
          child: Text(
            "SAVE",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );

    // Profile Information
    var accountInfo =
        new Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      new ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(),
            child: Container(
              decoration: new BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 5),
                    child: new Column(
                      children: <Widget>[
                        new TextFormField(
                          controller: nameController,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "SFProDisplay-Medium",
                              letterSpacing: .35),
                          decoration: new InputDecoration(
                              errorText:
                                  _validName ?  null : "Field can't be Empty",
                              errorStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 10,
                                fontFamily: "SFProDisplay-Medium",
                                letterSpacing: .35,
                              ),
                              hintText: 'Name',
                              disabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontFamily: "SFProDisplay-Medium",
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 40.0, bottom: 40.0),
                    child: saveButton,
                  ),
                ],
              ),
            ),
          ),
        ],
      )
    ]);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            initialBlock,
            accountInfo,
          ],
        ),
      ),
    );
  }

  @override
  userImage(File _imageFilePath) async {
    setState(() {
      this._imageFilePath = _imageFilePath;
    });
  }
}

class ImagePickerHandler {
  ImagePickerDialog imagePicker;
  AnimationController _controller;
  ImagePickerListener _listener;
  ImagePickerHandler(this._listener, this._controller);

  openCamera() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, maxHeight: 480, maxWidth: 640);
    if (image != null) {
      File cropped = await ImageCropper.cropImage(
          sourcePath: image.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          compressQuality: 100,
          maxWidth: 700,
          maxHeight: 700,
          compressFormat: ImageCompressFormat.jpg,
          androidUiSettings: AndroidUiSettings(
            toolbarColor: Colors.deepOrange,
            toolbarTitle: "Cropper",
            statusBarColor: Colors.deepOrange.shade900,
            backgroundColor: Colors.white,
          ),
          iosUiSettings: IOSUiSettings(
            title: 'Cropper',
          ));
      _listener.userImage(cropped);
    } else {}
  }

  openGallery() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 480, maxWidth: 640);
    if (image != null) {
      File cropped = await ImageCropper.cropImage(
          sourcePath: image.path,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          compressQuality: 100,
          maxWidth: 700,
          maxHeight: 700,
          compressFormat: ImageCompressFormat.jpg,
          androidUiSettings: AndroidUiSettings(
            toolbarColor: Colors.deepOrange,
            toolbarTitle: "Cropper",
            statusBarColor: Colors.deepOrange.shade900,
            backgroundColor: Colors.white,
          ),
          iosUiSettings: IOSUiSettings(
            title: 'Cropper',
          ));
      _listener.userImage(cropped);
    } else {}
  }

  void init() {
    imagePicker = new ImagePickerDialog(this, _controller);
    imagePicker.initState();
  }

  showDialog(BuildContext context) {
    imagePicker.getImage(context);
  }
}

abstract class ImagePickerListener {
  userImage(File _image);
}

// ignore: must_be_immutable
class ImagePickerDialog extends StatelessWidget {
  ImagePickerHandler _listener;
  AnimationController _controller;
  BuildContext context;

  ImagePickerDialog(this._listener, this._controller);

  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;

  void initState() {
    _drawerContentsOpacity = new CurvedAnimation(
      parent: new ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = new Tween<Offset>(
      begin: const Offset(0.0, 1.0),
      end: Offset.zero,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
  }

  getImage(BuildContext context) {
    if (_controller == null ||
        _drawerDetailsPosition == null ||
        _drawerContentsOpacity == null) {
      return;
    }
    _controller.forward();
    showDialog(
      context: context,
      builder: (BuildContext context) => new SlideTransition(
        position: _drawerDetailsPosition,
        child: new FadeTransition(
          opacity: new ReverseAnimation(_drawerContentsOpacity),
          child: this,
        ),
      ),
    );
  }

  void dispose() {
    _controller.dispose();
  }

  startTime() async {
    var _duration = new Duration(milliseconds: 200);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pop(context);
  }

  dismissDialog() {
    _controller.reverse();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    this.context = context;
    return new Material(
        type: MaterialType.transparency,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: height * 0.56),
            ),
            new Opacity(
              opacity: 1.0,
              child: new Container(
                margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 5.0),
                decoration: new BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.black, blurRadius: 6.0)
                    ]),
                padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 20.0,
                        ),
                        new GestureDetector(
                          onTap: () => _listener.openCamera(),
                          child: Column(
                            children: <Widget>[
                              new Icon(
                                Icons.camera_alt,
                                color: Colors.blue,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              new Text(
                                'Camera',
                                style: TextStyle(
                                    color: Colors.blue, fontSize: 15.0),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        new GestureDetector(
                          onTap: () => _listener.openGallery(),
                          child: Column(
                            children: <Widget>[
                              new Icon(
                                Icons.image,
                                color: Colors.blue,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              new Text(
                                'Gallery',
                                style: TextStyle(
                                    color: Colors.blue, fontSize: 15.0),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                      ],
                    ),
                    const SizedBox(height: 15.0),
                    new GestureDetector(
                        onTap: () => dismissDialog(),
                        child: Center(
                          child: Container(
                            height: 48.0,
                            width: 200.0,
                            child: RaisedButton(
                              onPressed: () {
                                dismissDialog();
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              padding: EdgeInsets.all(0.0),
                              child: Ink(
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Container(
                                  constraints: BoxConstraints(
                                      maxWidth: 350.0, minHeight: 50.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "CANCEL",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
