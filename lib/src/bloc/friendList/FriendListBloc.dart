import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_challenge/src/bloc/friendList/FriendModel.dart';
import 'package:flutter_challenge/src/utils/CommonMethods.dart';

class FriendListBloc{
  final friendListController = StreamController<FriendModel>();
  void init(BuildContext context)async{
   var response = await CommonMethods.readJsonFile(context, 'assets/screen_no_2_all_room.json');
   friendListController.sink.add( FriendModel.fromJson(response));
  }

  void dispose(){
    friendListController.close();
  }




}