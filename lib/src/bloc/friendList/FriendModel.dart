
import 'package:flutter_challenge/src/utils/CommonMethods.dart';

class FriendModel {
  final List<Friend> friends;

  FriendModel({
    this.friends,
  });

  factory FriendModel.fromJson(Map<String, dynamic> json) {
    return FriendModel(
        friends: List<Friend>.from(
            json['friends'].map((i) => Friend.fromJson(i))));
  }
}

class Friend {
  final String name;
  final String image;
  final String lastMessage;


  Friend({ this.name, this.image, this.lastMessage});

  factory Friend.fromJson(Map<String, dynamic> json) {
    return new Friend(
      name: CommonMethods.valueOrDefault(json['friend']['name']),
      image: CommonMethods.valueOrDefault(json['friend']['image']),
      lastMessage: CommonMethods.valueOrDefault(json['friend']['lastMessage']),

    );
  }
}
