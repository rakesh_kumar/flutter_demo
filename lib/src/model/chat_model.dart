class ChatModel{
  String messageFrom;
  String messageContent;
  String messageType;
  String messageFileType;
  String messageFileName;
  String messageRoomId;
  String messageDate;
  String messageSenderId;
  String messageRoomName;
  String messageLive;

  ChatModel({this.messageFrom, this.messageContent, this.messageType, this.messageFileType, this.messageFileName, this.messageRoomId, this.messageDate, this.messageSenderId, this.messageRoomName, this.messageLive});
}