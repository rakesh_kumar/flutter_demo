import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/bloc/friendList/FriendListBloc.dart';
import 'package:flutter_challenge/src/bloc/friendList/FriendModel.dart';
import 'package:flutter_challenge/src/room/room.dart';

class FriendListScreen extends StatefulWidget {
  static const tag = 'friendList';
  FriendListScreen({Key key}) : super(key: key);

  @override
  _FriendListScreenState createState() => _FriendListScreenState();
}

class _FriendListScreenState extends State<FriendListScreen> {
  final friendListBloc = FriendListBloc();
  @override
  void initState() {
    friendListBloc.init(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<FriendModel>(
          stream: friendListBloc.friendListController.stream,
          builder: (context, snapshot) {
            return snapshot.hasData
                ? Container(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return cardView(snapshot.data, index);
                      },
                      itemCount: snapshot.data.friends.length,
                    ),
                  )
                : CircularProgressIndicator();
          }),
    );
  }

  Widget cardView(FriendModel chatModel, int index) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 5, right: 5),
      child: CupertinoButton(
        padding: EdgeInsets.all(0),
        onPressed: ()=> Navigator.pushNamed(context, RoomScreen.tag),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            radius: 20,
                            backgroundImage: NetworkImage(chatModel.friends[index].image),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text( chatModel.friends[index].name),
                          Text(chatModel.friends[index].lastMessage),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
