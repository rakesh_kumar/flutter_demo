import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/model/chat_model.dart';
import 'package:intl/intl.dart';

class RoomScreen extends StatefulWidget {
  static const tag = 'room';

  RoomScreen({Key key}) : super(key: key);

  @override
  _RoomScreenState createState() => _RoomScreenState();
}

class _RoomScreenState extends State<RoomScreen> {
  final TextEditingController textEditingController =
      new TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  final FocusNode focusNode = new FocusNode();
  int counter = 0;
  List<ChatModel> messageDataList = new List<ChatModel>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  sendMessage(String message, int count) {
    ChatModel chatModel = ChatModel();
    chatModel.messageContent = message;
    chatModel.messageDate = DateTime.now().toString();
    setState(() {
      messageDataList.add(chatModel);
    });
  }

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    //double deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: new AppBar(title: const Text('Room')),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              // List of messages
              buildListMessage(),
              // Input content
              buildInput(),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildItem(int index, ChatModel chatModel) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
        child: Column(
      children: <Widget>[
        SizedBox(height: 20),
        Container(
          width: MediaQuery.of(context).size.width*0.9,
          child: Text(
            chatModel.messageContent,
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.grey, fontSize: 14.0),
          ),
        ),
        Container(
          child: Row(
            children: <Widget>[
              Text(
                DateFormat('dd MMM hh:mm a')
                    .format(DateTime.parse(chatModel.messageDate)),
                textAlign: TextAlign.right,
                style: TextStyle(color: Colors.grey, fontSize: 10.0),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.end,
          ),
          margin: EdgeInsets.only(right: 10.0, top: 3.0),
        ),
      ],
    ));
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 10.0,
          ),
          // Edit text
          Expanded(
            child: TextField(
              maxLines: null,
              style: TextStyle(color: Colors.black, fontSize: 15.0),
              textInputAction: TextInputAction.newline,
              controller: textEditingController,
              decoration: InputDecoration.collapsed(
                hintText: 'Send a message',
              ),
              focusNode: focusNode,
            ),
          ),

          // Button send message
          Material(
            child: new Container(
              margin: new EdgeInsets.only(right: 5.0),
              child: new IconButton(
                icon: Icon(
                  Icons.send,
                  color: const Color(0xFF000080),
                ),
                onPressed: () {
                  Timer(
                      Duration(milliseconds: 500),
                      () => listScrollController.jumpTo(
                          listScrollController.position.maxScrollExtent));
                  counter++;
                  sendMessage(textEditingController.text, counter);
                  textEditingController.clear();
                },
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      constraints: BoxConstraints(minWidth: double.infinity, maxHeight: 110.0),
      decoration: new BoxDecoration(
          border:
              new Border(top: new BorderSide(color: Colors.grey, width: 0.5)),
          color: Colors.white),
    );
  }

  Widget buildListMessage() {
    return Flexible(
        child: ListView.builder(
      padding: EdgeInsets.all(10.0),
      itemBuilder: (context, index) => buildItem(index, messageDataList[index]),
      itemCount: messageDataList != null ? messageDataList.length : 0,
      reverse: false,
      controller: listScrollController,
    ));
  }
}
