import 'dart:convert';

import 'package:flutter/material.dart';

class CommonMethods{


  static valueOrDefault(var data) {
    if (data != null) {
      return data;
    } else {
      if (data.runtimeType == bool) {
        return false;
      } else if (data.runtimeType == int) {
        return 0;
      } else if (data.runtimeType == String) {
        return "";
      }
      return "";
    }
  }

  static readJsonFile(BuildContext context, String path)async{
    String data = await DefaultAssetBundle.of(context).
    loadString(path);
    final jsonResult = json.decode(data);
    return jsonResult;
  }



}