import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/Routers.dart';
import 'package:flutter_challenge/src/user_repository.dart';

class App extends StatefulWidget {
  final UserRepository userRepository;
  App({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:  false,
      theme: ThemeData(
        accentColor: Colors.blue
      ),
      routes: Routers.allRoutes(),
      initialRoute: Routers.initialRoute(),
    );
  }
}
