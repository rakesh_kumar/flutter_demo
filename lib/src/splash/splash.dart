import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_challenge/src/bottomBar/bottom_navigation_bar.dart';
import 'package:flutter_challenge/src/login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const tag = 'splash';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final splashDelay = 5;

  @override
  void initState() {
    super.initState();
    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  Future<void> navigationPage() async {
    SharedPreferences prefer =
        await SharedPreferences.getInstance();
    if (prefer.getBool("isLoggedIn")){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => BottomNavigationBarScreen()));
    }else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        child: Center(
          child: Text('SPLASH',
          style: TextStyle(fontSize: 40, color: Colors.black),
        ),
        )
      ),
    );
  }
}